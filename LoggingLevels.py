LOGGING_LEVELS = (('debugging', 7), ('informational', 6), ('notifications', 5), ('warnings', 4),
    ('errors', 3), ('critical', 2), ('alerts', 1), ('emergencies', 0))

def logging_levels_generator():
    for level in LOGGING_LEVELS:
        yield level
        