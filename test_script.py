import re
import logging
from pyats import aetest
from pyats.topology import loader
from LoggingLevels import logging_levels_generator
from connect.connect import Connect


logger = logging.getLogger(__name__)


class CommonSetup(aetest.CommonSetup):

    @aetest.subsection
    def load_topology(self):
        testbed = loader.load('testbed.yaml')
        SOHO = testbed.devices['SOHO']
        ISP1 = testbed.devices['ISP1']
        ISP2 = testbed.devices['ISP2']
        ISP3 = testbed.devices['ISP3']
        ciscoIOS = testbed.devices['CiscoIOSv15.6(1)T-1']
        ubuntu = testbed.devices['UbuntuServer18.04']
        self.parent.parameters.update(testbed = testbed)
        self.parent.parameters.update(SOHO = SOHO)
        self.parent.parameters.update(ISP1 = ISP1)
        self.parent.parameters.update(ISP2 = ISP2)
        self.parent.parameters.update(ISP3 = ISP3)
        self.parent.parameters.update(ciscoIOS = ciscoIOS)
        self.parent.parameters.update(ubuntu = ubuntu)

    @aetest.loop(device = ('SOHO', 'ISP1', 'ISP2', 'ISP3', 'ubuntu', 'ciscoIOS'))
    @aetest.subsection
    def connect_to_devices(self, device):
        device = self.parameters[device]
        try:
            device.connect()
        except Exception as e:
            self.failed(f'Connecting to device {device.name} failed with eror {str(e)}')

    @aetest.loop(device = ('SOHO', 'ISP1', 'ISP2', 'ISP3'))
    @aetest.subsection
    def backup_config(self, device):       
        device = self.parameters[device]
        device.configure('file prompt quiet')
        filename = device.name.lower() + '-confg'
        device.execute(f'copy running-config tftp://192.168.122.85/{filename}')

    @aetest.loop(device = ('SOHO', 'ISP1', 'ISP2', 'ISP3'))
    @aetest.subsection
    def set_clean_config(self, device):
        device = self.parameters[device]
        device.execute(f'copy tftp://192.168.122.85/clean-confg running-config')

    @aetest.subsection
    def set_clean_config(self, ciscoIOS):
        ciscoIOS.execute('copy tftp://192.168.122.85/clean-confg-ios running-config')

    @aetest.loop(device = ('SOHO', 'ISP1', 'ISP2', 'ISP3', 'ciscoIOS'))
    @aetest.subsection
    def configure_routers(self, device):
        device = self.parameters[device]
        device.execute('term len 0')
        device.execute('term width 511')
        configuration_str = ''
        for intf in device.interfaces.values():
            name = intf.name
            ipv4 = intf.ipv4.ip
            mask = intf.ipv4.netmask
            configuration_str += f'int {name}\n'
            configuration_str += f'ip address {ipv4} {mask}\n'
            configuration_str += 'ip ospf hello-interval 5\n'
            configuration_str += 'ip ospf dead-interval 20\n'
            configuration_str += 'no shut\n'
            configuration_str += 'exit\n'
        device.configure(configuration_str)

    @aetest.loop(device = ('SOHO', 'ISP1', 'ISP2', 'ISP3', 'ciscoIOS'))
    @aetest.subsection
    def configure_ospf(self, device):
        device = self.parameters[device]
        configuration_str = ''
        configuration_str += 'router ospf 1\n'
        for intf in device.interfaces.values():
            network = intf.ipv4.network.network_address
            reversed_mask = intf.ipv4.hostmask  
            configuration_str += f'network {network} {reversed_mask} area 0\n'
        device.configure(configuration_str)


@aetest.loop(device = ('SOHO', 'ISP1', 'ISP2', 'ISP3'))
class Testcase1(aetest.Testcase):
    '''
    Test possibility of routers to ping every interface in topology
    PASS CRITERIA: success rate of ping is equal or more than 60%
    FAIL CRITERIA: success rate of ping is lower than 60%
    '''
    @aetest.setup
    def setup(self, testbed):
        destination = []
        for device in testbed.devices.values():
            for int in device.interfaces.values():
                ip = int.ipv4.ip
                if ip not in destination:
                    destination.append(int.ipv4.ip)
        aetest.loop.mark(self.ping, destination = destination)

    @aetest.test
    def ping(self, device, destination):
        print('Entered method ping')
        try:
            result = self.parameters[device].ping(destination)
        except Exception as e:
            self.failed(f'Ping {destination} from device {device} failed with eror: {str(e)}')
        else: 
            match = re.search(r'Success rate is (?P<rate>\d+) percent', result)
            success_rate = match.group('rate')
            logger.info(f'Success rate of {device} pinging {destination} is {success_rate}%')
            if int(success_rate) >= 60:
                self.passed('Success rate is more or equal 60%, test passed')
            else:
                self.failed('Success rate is less than 60%, test failed')


@aetest.loop(device = ('SOHO', 'ISP1', 'ISP2', 'ISP3'))
class Testcase2(aetest.Testcase):
    '''
    Compare information from |show ip interface brief| and |show run interface <name_of_interface>|
    PASS CrITErIA: IPs from both commands are equal
    FAIL CrITErIA: at least one IP is not equal
    '''
    
    @aetest.setup
    def setup(self):
        pass
        
    @aetest.test
    def validate(self, device):
        device = self.parameters[device]

        data = device.execute('show ip interface brief')
        ip_int_results = re.findall(r'(\w+\d/\d)\s+(\d+\.\d+\.\d+\.\d+)', data)
        if not ip_int_results:
            self.failed(f'Wrong execution of command "show ip interface brief" on device {device.name}', goto=['next_tc'])

        run_int_results = []
        for interfaces_info in ip_int_results:
            interface_name = interfaces_info[0]
            data = device.execute(f'show run interface {interface_name}')
            ip = re.findall(r'ip\saddress\s(\d+\.\d+\.\d+\.\d+)', data)[0]
            if not ip:
                self.failed(f'Wrong execution of command "show ip run {interface_name}" on device {device.name}', 
                goto=['next_tc'])
            run_int_results.append((interfaces_info[0], re.findall(r'ip\saddress\s(\d+\.\d+\.\d+\.\d+)', data)[0]))
        if len(ip_int_results) != len(run_int_results):
            self.failed(f'Length of interfaces from 2 commands is not equal on device {device}\n\
                list of interfaces according to <show ip interface brief>:\n{ip_int_results}\n\
                    list of interfaces according to <show run interface <name_of_interface>:\n{run_int_results}',
                    goto=['next_tc'])
        flag = True
        for i in range(len(ip_int_results)):
            interface_name = ip_int_results[i][0]
            ip_int = ip_int_results[i][1]
            ip_run = run_int_results[i][1]
            if ip_int != ip_run:
                flag = False
                logger.eror(f'IP of interface {interface_name} is not equal for commands <show ip interface brief>\
                    and <show run interface {interface_name}>:\n {ip_int} != {ip_run}')
        if not flag:
            self.failed('Testcase failed, IPs from commands commands <show ip interface brief>\
                    and <show run interface interface_name> are not equal', goto=['next_tc'])


@aetest.loop(device = ('SOHO', 'ISP1', 'ISP2', 'ISP3'))
class Testcase3(aetest.Testcase):
    '''
    Execute different kind of actions to generate logs and check whether they are received by syslog server on ubuntu
    Levels of logging to be tested: debugging, informational, notifications, errors
    PASS CRITERIA: proper messages from router are logged in syslog server on ubuntu
    FAIL CRITERIA: messages are not logged or wrong messages are logged 
    '''

    LOOP_NUMBER = 10

    @aetest.setup
    def setup(self, ubuntu, device):
        syslog_address = str(ubuntu.interfaces.ens0.ipv4.ip)
        result = self.parameters[device].ping(syslog_address)
        match = re.search(r'Success rate is (?P<rate>\d+) percent', result)
        success_rate = match.group('rate')
        if int(success_rate) >= 40:
            self.passed('Success rate of connection to syslog server is more or equal 40%, moving forward')
        else:
            self.failed('Success of connection to syslog server rate is less than 40%, stopping testcase')
        device = self.parameters[device]
        configure_str = f'''
        logging host {syslog_address}
        logging on
        logging trap debugging
        '''
        device.configure(configure_str)

    @aetest.test
    def check_send_log(self, ubuntu, device):
        device = self.parameters[device]
        device_address = str(device.interfaces['FastEthernet0/0'].ipv4.ip)
        log_message = 'This is message to test if send log command works!'
        device.execute(f'send log "{log_message}"')
        for i in range(self.LOOP_NUMBER):
            result = ubuntu.execute(f'tail --lines=5 /var/log/topology/{device_address}.log | grep "{log_message}"')
            if result:
                self.passed('Command "send log" does work')
        self.failed('Command "send log" does not work')

    @aetest.test
    def check_debugging_level(self, ubuntu, device):
        device = self.parameters[device]
        syslog_address = str(ubuntu.interfaces.ens0.ipv4.ip)
        device_address = str(device.interfaces['FastEthernet0/0'].ipv4.ip)
        log_message = f'ICMP: echo reply rcvd, src {syslog_address}, dst {device_address}'
        configure_str = 'logging trap debugging'
        device.configure(configure_str)
        device.execute('debug ip icmp')
        device.execute(f'ping {syslog_address}')
        for i in range(self.LOOP_NUMBER):
            result = ubuntu.execute(f'tail --lines=5 /var/log/topology/{device_address}.log | grep "{log_message}"') 
            if result:
                self.passed('Debugging logs does work')
        self.failed('Debugging logs does not work')

    @aetest.test
    def check_informational_level(self, ubuntu, device):
        device = self.parameters[device]
        syslog_address = str(ubuntu.interfaces.ens0.ipv4.ip)
        device_address = str(device.interfaces['FastEthernet0/0'].ipv4.ip)
        log_message = f'list 122 permitted icmp {syslog_address} -> {device_address}'
        configure_str = f'''
        logging trap informational
        logging host {syslog_address}
        logging on
        access-list 122 permit ip any any log 
        int fa0/0
        ip access-group 122 in
        '''
        device.configure(configure_str)
        ubuntu.execute(f'ping -c 10 {device_address}')
        device.execute('send log test')
        for i in range(10):
            result = ubuntu.execute(f'tail --lines=5 /var/log/syslog | grep "{log_message}"') 
            if result:
                self.passed('Information logs does work')
        self.failed('Information logs does not work')

    @aetest.test
    def check_notifications_level(self, ubuntu, device):
        device = self.parameters[device]
        device_address = str(device.interfaces['FastEthernet0/0'].ipv4.ip)
        log_message = 'Line protocol on Interface FastEthernet2/0, changed state to down'
        configure_str = f'''logging trap notifications
        int fa2/0
        shut
        no shut
        '''
        device.configure(configure_str)
        for i in range(10):
            result = ubuntu.execute(f'tail --lines=5 /var/log/topology/{device_address}.log | grep "{log_message}"') 
            if result:
                self.passed('Notifications logs does work')
        self.failed('Notifications logs does not work')
     
    @aetest.test
    def check_errors_level(self, ubuntu, device):
        device = self.parameters[device]
        syslog_address = str(ubuntu.interfaces.ens0.ipv4.ip)
        device_address = str(device.interfaces['FastEthernet0/0'].ipv4.ip)
        log_message = 'Interface FastEthernet2/0, changed state to up'
        configure_str = f'''logging trap errors
        int fa2/0
        shut
        no shut
        '''
        device.configure(configure_str)
        for i in range(10):
            result = ubuntu.execute(f'tail --lines=25 /var/log/topology/{device_address}.log | grep "{log_message}"') 
            if result:
                self.passed('Errors logs does work')
        self.failed('Errors logs does not work')


class Testcase4(aetest.Testcase):
    '''
    Execute command <send log> with different levels of logging(severity) and check whether this message is logged
    in syslog server on ubuntu with proper severity
    Levels of logging to be tested: debugging, informational, notifications, warnings, errors, critical, alerts, emergencies
    PASS CRITERIA: proper messages from router are logged in syslog server on ubuntu
    FAIL CRITERIA: messages are not logged or wrong messages are logged 
    '''

    LOOP_NUMBER = 10

    @aetest.setup
    def setup(self, ubuntu, ciscoIOS):
        syslog_address = str(ubuntu.interfaces.ens0.ipv4.ip)
        configure_str = f'''
        logging host {syslog_address}
        logging on
        '''
        ciscoIOS.configure(configure_str)

    @aetest.loop(level = logging_levels_generator())
    @aetest.test
    def test_level(self, ubuntu, ciscoIOS, level):
        level_name = level[0]
        severity = level[1]
        short_level_name = level_name[:3].upper()
        log_message = f'{level_name} level test message'
        severity_message = f'%SYS-{severity}-USERLOG_{short_level_name}'
        configure_str = f'logging trap {level_name}'
        ciscoIOS.configure(configure_str)
        ciscoIOS.execute(f'send log {severity} {log_message}')
        for i in range(self.LOOP_NUMBER):
            result = ubuntu.execute(f'tail --lines=5 /var/log/syslog | grep "{log_message}"') 
            if result and severity_message in result:
                self.passed(f'{level_name} level of logging using command <send log> does work')
        self.failed(f'{level_name} level of logging using command <send log> does not work')   


class Testcase5(aetest.Testcase):
    '''
    Configure ssh on router and check if it works from ubuntu
    PASS CRITERIA: ubuntu have ability to login on router using ssh
    FAIL CRITERIA: ubuntu does not have ability to login on router using ssh, router ssh configuration is wrong
    '''

    @aetest.setup
    def setup(self, ciscoIOS):
        configure_str = f'''
        hostname ciscoIOS
        ip domain-name ADMIN.LOCAL
        crypto key generate rsa modulus 1024
        ip ssh version 2
        line vty 0 4
        transport input ssh
        login local
        exit
        username admin password admin
        '''
        ciscoIOS.configure(configure_str)

    @aetest.test
    def test_ssh_connection(self, ubuntu):
        ssh_connection = Connect(ubuntu)
        ssh_connection.connect()
        ver = ssh_connection.get_version()
        ssh_connection.disconnect()
        if ver:
            self.passed('Succesfully connected to router from ubuntu using ssh. Router version: {ver}')
        self.failed('Failed to connect to router using ssh or wrong execution of <show version command>')


class CommonCleanup(aetest.CommonCleanup): 

    @aetest.loop(device = ('SOHO', 'ISP1', 'ISP2', 'ISP3', 'ciscoIOS'))
    @aetest.subsection
    def reset_config(self, device):
        device = self.parameters[device]
        filename = device.name.lower() + '-confg'
        device.execute(f'copy tftp://192.168.122.85/{filename} running-config')
        device.execute('wr')

    @aetest.loop(device = ('SOHO', 'ISP1', 'ISP2', 'ISP3', 'ubuntu', 'ciscoIOS'))
    @aetest.subsection
    def disconnect(self, device):
        device = self.parameters[device]
        try:
            device.disconnect()
        except Exception as e:
            self.failed(f'Disconnecting from device {device.name} failed with eror {str(e)}')
        

if __name__ == '__main__':
    aetest.main()
