import re


class Connect:
    '''
    Class for connecting to router from ubuntu using ssh and executing command there
    '''
    def __init__(self, ubuntu):
        self.ubuntu = ubuntu

    def connect(self):
        self.ubuntu.execute('ssh-keygen -f "/home/gns3/.ssh/known_hosts" -R "192.168.122.99"')
        try: 
            self.ubuntu.send('ssh admin@192.168.122.99\r')
            self.ubuntu.expect('Password')
            self.ubuntu.send('admin\r')
        except Exception as e:
            self.ubuntu.expect('continue connecting')
            self.ubuntu.send('yes\r')
            self.ubuntu.expect('Password')
            self.ubuntu.send('admin\r')

    def get_version(self):
        self.ubuntu.execute('term len 0')
        data = self.ubuntu.execute('show version')
        showed_version = re.findall(r', Version (.+),', data)
        parsed_version = ''
        if showed_version:
            parsed_version = showed_version[0]
        return parsed_version

    def disconnect(self):
        self.ubuntu.execute('exit')
